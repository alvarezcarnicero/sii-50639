#include "Mundo.h"

class Coordinador{
public:
	Coordinador();
	~Coordinador();
	void tecla(unsigned char key);
	void mueve();
	void dibuja();
	Mundo mundo;
	
protected:
	enum Estado {INICIO,JUEGO,FIN}
	Estado estado;
};

