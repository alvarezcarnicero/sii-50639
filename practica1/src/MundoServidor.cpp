// Mundo.cpp: implementation of the CMundo class.
//
//////////////////////////////////////////////////////////////////////
#include <fstream>
#include "MundoServidor.h"

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <math.h>
#include <unistd.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <fcntl.h>
#include <sys/mman.h>
#include <signal.h>
#include <error.h>
#include "glut.h"

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

//char* org;
void* hilo_comandos(void* d);

MundoServidor::MundoServidor()
{
	Init();
	
}

MundoServidor::~MundoServidor()
{
	close(fifologger);
	unlink("/tmp/fifoLogger");
	close(fifocs);
	unlink("/tmp/fifo_c-s");
	close(fifoteclas);
	unlink("/tmp/fifoTeclas");
	
}

void MundoServidor::InitGL()
{
	//Habilitamos las luces, la renderizacion y el color de los materiales
	glEnable(GL_LIGHT0);
	glEnable(GL_LIGHTING);
	glEnable(GL_DEPTH_TEST);
	glEnable(GL_COLOR_MATERIAL);
	
	glMatrixMode(GL_PROJECTION);
	gluPerspective( 40.0, 800/600.0f, 0.1, 150);
}

void print(char *mensaje, int x, int y, float r, float g, float b)
{
	glDisable (GL_LIGHTING);

	glMatrixMode(GL_TEXTURE);
	glPushMatrix();
	glLoadIdentity();

	glMatrixMode(GL_PROJECTION);
	glPushMatrix();
	glLoadIdentity();
	gluOrtho2D(0, glutGet(GLUT_WINDOW_WIDTH), 0, glutGet(GLUT_WINDOW_HEIGHT) );

	glMatrixMode(GL_MODELVIEW);
	glPushMatrix();
	glLoadIdentity();

	glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
	glDisable(GL_DEPTH_TEST);
	glDisable(GL_BLEND);
	glColor3f(r,g,b);
	glRasterPos3f(x, glutGet(GLUT_WINDOW_HEIGHT)-18-y, 0);
	int len = strlen (mensaje );
	for (int i = 0; i < len; i++) 
		glutBitmapCharacter (GLUT_BITMAP_HELVETICA_18, mensaje[i] );
		
	glMatrixMode(GL_TEXTURE);
	glPopMatrix();

	glMatrixMode(GL_PROJECTION);
	glPopMatrix();

	glMatrixMode(GL_MODELVIEW);
	glPopMatrix();

	glEnable( GL_DEPTH_TEST );
}

void MundoServidor::OnDraw()
{
	//Borrado de la pantalla	
   	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	//Para definir el punto de vista
	glMatrixMode(GL_MODELVIEW);	
	glLoadIdentity();
	
	gluLookAt(0.0, 0, 17,  // posicion del ojo
		0.0, 0.0, 0.0,      // hacia que punto mira  (0,0,0) 
		0.0, 1.0, 0.0);      // definimos hacia arriba (eje Y)    

	///////////////
	///////////
	//		AQUI EMPIEZA MI DIBUJO
	char cad[100];
	char t[20];
	sprintf(cad,"Jugador1: %d",puntos1);
	print(cad,10,0,1,1,1);
	sprintf(cad,"Servidor");
	print(t,375,0,1,1,1);
	sprintf(cad,"Jugador2: %d",puntos2);
	print(cad,650,0,1,1,1);
	int i;
	for(i=0;i<paredes.size();i++)
		paredes[i].Dibuja();

	fondo_izq.Dibuja();
	fondo_dcho.Dibuja();
	jugador1.Dibuja();
	jugador2.Dibuja();
	esfera.Dibuja();

	/////////////////
	///////////
	//		AQUI TERMINA MI DIBUJO
	////////////////////////////

	//Al final, cambiar el buffer
	glutSwapBuffers();
}

void MundoServidor::OnTimer(int value)
{	
	
	char cad[200];
	sprintf(cad,"%f %f %f %f %f %f %f %f %f %f %f %d %d", esfera.centro.x,esfera.centro.y,esfera.radio, jugador1.x1,jugador1.y1,jugador1.x2,jugador1.y2,jugador2.x1,jugador2.y1,jugador2.x2,jugador2.y2, puntos1, puntos2);
	write(fifocs,cad,sizeof(cad));


	jugador1.Mueve(0.025f);
	jugador2.Mueve(0.025f);
	esfera.Mueve(0.025f);
	esfera.Reduce(0.025f);
	int i;

	for(i=0;i<paredes.size();i++)
	{
		paredes[i].Rebota(esfera);
		paredes[i].Rebota(jugador1);
		paredes[i].Rebota(jugador2);
	}

	jugador1.Rebota(esfera);
	jugador2.Rebota(esfera);

	if(fondo_izq.Rebota(esfera))
	{
		esfera.centro.x=0;
		esfera.centro.y=rand()/(float)RAND_MAX;
		esfera.radio=1.0f;
		esfera.velocidad.x=2+2*rand()/(float)RAND_MAX;
		esfera.velocidad.y=2+2*rand()/(float)RAND_MAX;
		puntos2++;
		char c[200];
		sprintf(c,"Jugador 2 marca 1 punto, lleva %d puntos\n", puntos2);
		write (fifologger, c, strlen(c)+1);
	}

	
	if(fondo_dcho.Rebota(esfera))
	{
		esfera.centro.x=0;
		esfera.radio=1.0f;
		esfera.centro.y=rand()/(float)RAND_MAX;
		esfera.velocidad.x=-2-2*rand()/(float)RAND_MAX;
		esfera.velocidad.y=-2-2*rand()/(float)RAND_MAX;
		puntos1++;
		char c[200];
		sprintf(c,"Jugador 1 marca 1 punto, lleva %d puntos\n", puntos1);
		write (fifologger, c, strlen(c)+1);
	}

	/*	switch(pMemC->accion){
		case 1:  OnKeyboardDown('w',0,0); break;
		case 0:  break;
		case -1: OnKeyboardDown('s',0,0); break;
	}
	pMemC->esfera=esfera;
	pMemC->raqueta1=jugador1;*/

	//fin juego
	if(puntos1==3 || puntos2==3)
		exit(0);

}

void MundoServidor::OnKeyboardDown(unsigned char key, int x, int y)
{
	switch(key)
	{
//	case 'a':jugador1.velocidad.x=-1;break;
//	case 'd':jugador1.velocidad.x=1;break;
	case 's':jugador1.velocidad.y=-4;break;
	case 'w':jugador1.velocidad.y=4;break;
	case 'l':jugador2.velocidad.y=-4;break;
	case 'o':jugador2.velocidad.y=4;break;
	}
}

void MundoServidor::Init()
{
	fifologger=open("/tmp/fifoLogger",O_WRONLY);
	fifocs=open("/tmp/fifo_c-s",O_WRONLY);
	fifoteclas=open("/tmp/fifoTeclas",O_RDONLY);
	pthread_create(&th,NULL,hilo_comandos,this);

	Plano p;
//pared inferior
	p.x1=-7;p.y1=-5;
	p.x2=7;p.y2=-5;
	paredes.push_back(p);

//superior
	p.x1=-7;p.y1=5;
	p.x2=7;p.y2=5;
	paredes.push_back(p);

	fondo_izq.r=0;
	fondo_izq.x1=-7;fondo_izq.y1=-5;
	fondo_izq.x2=-7;fondo_izq.y2=5;

	fondo_dcho.r=0;
	fondo_dcho.x1=7;fondo_dcho.y1=-5;
	fondo_dcho.x2=7;fondo_dcho.y2=5;

	//a la izq
	jugador1.g=0;
	jugador1.x1=-6;jugador1.y1=-1;
	jugador1.x2=-6;jugador1.y2=1;
	MemC.raqueta1=jugador1;

	//a la dcha
	jugador2.g=0;
	jugador2.x1=6;jugador2.y1=-1;
	jugador2.x2=6;jugador2.y2=1;

	/*int file=open("/tmp/datosComp.txt",O_RDWR|O_CREAT|O_TRUNC, 0666);
	write(file,&MemC,sizeof(MemC));
	org=(char*)mmap(NULL,sizeof(MemC),PROT_WRITE|PROT_READ,MAP_SHARED,file,0);
	close(file);
	pMemC=(DatosMemCompartida*)org;
	pMemC->accion=0;*/
	
	
}

void* hilo_comandos(void* d)
{
	MundoServidor* p=(MundoServidor*) d;
	p->Comandos();
}

void MundoServidor::Comandos()

{

     while (1) {
          usleep(10);
          char cad[100];
          read(fifoteclas,cad,sizeof(cad));
          unsigned char key;
          sscanf(cad,"%c",&key);
          if(key=='s')jugador1.velocidad.y=-3;
          if(key=='w')jugador1.velocidad.y=3;
          if(key=='l')jugador2.velocidad.y=-3;
          if(key=='o')jugador2.velocidad.y=3;

     }
}
