// Esfera.cpp: implementation of the Esfera class.
//
//////////////////////////////////////////////////////////////////////

#include "Esfera.h"
#include "glut.h"
//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

Esfera::Esfera()
{
	radio=0.9f;
	radiomin=0.25f;
	velocidad.x=3;
	velocidad.y=3;
	time=5.0f;
	tt=0;
}

Esfera::~Esfera()
{

}



void Esfera::Dibuja()
{
	glColor3ub(255,255,0);
	glEnable(GL_LIGHTING);
	glPushMatrix();
	glTranslatef(centro.x,centro.y,0);
	glutSolidSphere(radio,15,15);
	glPopMatrix();
}

void Esfera::Mueve(float t)
{
	centro=centro+velocidad*t+aceleracion*t*t*0.5;
	velocidad=velocidad+aceleracion*t;
}

void Esfera::Reduce(float t)
{
tt+=t;
	if(time<tt)
	{
		if(radio>radiomin)
		{
			radio-=0.2;
			tt=0;
		}
	}
}
